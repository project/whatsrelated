Warning
--------

Batteries not included, some assembly required.  Right now I'm using
this by displaying related links through an addition to "node.tpl.php"
in a phptemplate theme.


Overview
--------
The whatsrelated.module allows you to create new nodes of the type
"whatsrelated".  It's a simple content type which defines an external
link, and the node ID of the pre-existing node you want to associate
it with.



Requirements
------------
Drupal 4.6.x
PHP 4.3.0 or greater


Installation
------------
Please refer to the INSTALL file for installation directions.


Credits
-------
 - Original author:  John Handelaar


Bugs and Suggestions
--------------------
Bug reports, support requests, feature requests, etc, should be posted to
banner module project page:
http://drupal.org/project/whatsrelated

