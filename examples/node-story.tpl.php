<div class="node<?php if ($sticky) { print " sticky"; } ?>">
    <?php if ($picture) { 
      print $picture;
    }?>
    <?php if ($page == 0): ?>
      <h2 class="title"><a href="<?php print $node_url ?>"><?php print $title?></a></h2>
    <?php endif; ?>
    <span class="taxonomy"><?php print $terms?></span>

    <div class="content"><?php print $content?></div>
    
 <?php if ($page): ?>
 
 <div id = "articleinfo">
    <?php if ($links): ?><div class="links"><?php print $links ?></div><?php endif; ?>
    <!-- <?php if ($statistics): ?><div class="links"><?php print $statistics ?></div><?php endif; ?> -->
    </div>
 
  <?php if (module_exist('whatsrelated')) : ?>
  <?php
  
  $whatsrelatedquery = db_query("SELECT node.title, node.nid, node.body, whatsrelated.relatedto, whatsrelated.linkaddress, whatsrelated.nid
    FROM node, whatsrelated
    WHERE whatsrelated.relatedto = '" . $node->nid . "'
    AND node.nid = whatsrelated.nid
    AND node.type = 'whatsrelated' 
    ORDER BY whatsrelated.nid DESC, node.title ASC");
  if (db_num_rows($whatsrelatedquery) > 0) {
    echo "<h2 class=\"linksh2\">Related links</h2>";
    while ($links = db_fetch_object($whatsrelatedquery)) {
      echo "<div class=\"relatedlink\">";
      echo "<p><strong><a href=\"" . $links->linkaddress . "\">" . $links->title . "</a></strong>";
      if (user_access('administer nodes')) {
      echo " (<a href=\"node/" . $links->nid . "/edit\">edit this link</a>)";
      }
      echo "<br />" . $links->body . "</p>";
      echo "</div>";
    }
  }
  ?>
  <?php endif; ?>
 <?php endif; ?>
 </div>
